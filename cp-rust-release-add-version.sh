#!/bin/bash

:<<EOF
复制编译文件并添加版本号

Version : 1.0
Create  : 2025-01-08
Author  : ahat_hua
EOF

read -p "请输入项目名：" pro_name
pro_path="/e/rswork/${pro_name}"
echo $pro_path
if test -d $pro_path; then
    app_file="${pro_path}/target/release/${pro_name}.exe"
    if [ -e $app_file ]; then
        ver_file="${pro_path}/_version"
        if [ -f $ver_file ]; then
            read -r version < $ver_file
            release="/d/Tools/me/${pro_name}/${version}"
            mkdir -p $release
            cp $app_file "${release}/${pro_name}.exe"
            echo $release
        else
            echo "没有版本号"
            exit -1
        fi
    else
        echo "项目未编译"
        exit -1
    fi
else
    echo "找不到项目"
    exit -1
fi
exit 0
