me实用shell脚本库
==

> 一些shell脚本，方便快速完成一些任务。

## 脚本列表

### git 版本库相关

`init_bare_repo.sh` 服务端快速创建Git裸仓库脚本。

`ch-git-email.sh` 修改git日志邮箱和用户。

### tree 目录树相关

`tree-build.sh` 生成项目目录树文件，并添加注释文本。

### docker 容器相关

`docker/cdw.sh` 切换到docker工作目录。

`docker/boot_list.sh` 启动所需的容器和重启一些服务。

`docker/restart_alpine.sh` 重启Alpine容器并重启容器的sshd服务。

`docker/restart_web.sh` 重启Web容器并重启容器的sshd服务。

`docker/start_docker_list.sh` 启动相关容器和执行一些命令。

`docker/stop_docker_list.sh` 停止相关容器。

### cp 复制相关

`cp-to.sh` 排除指定文件或目录后，复制所有。

`cp-rust-release-add-version.sh` 复制编译文件并添加版本号。
