#!/bin/bash

# 启动docker所需的容器和命令
echo '开始停止一堆东西...'
docker stop ngui &
docker stop nginx &
docker stop php001 &
docker stop php002 &
docker stop static &
docker stop php74fpm &
docker stop mysql57 &
docker stop mariadb &
docker stop mysql801 &
docker stop web &
docker stop alpine &
wait
docker ps
echo '一堆东西已经停止完毕'
