#!/bin/bash

# 启动所需的容器和命令
function step1(){
	docker start php74fpm
	docker start php001
	docker start php002
	docker start nginx
}
function step2(){
	docker start web
	docker exec web /etc/init.d/sshd restart
}
function step3(){
	docker start alpine
	docker exec alpine /etc/init.d/sshd restart
}
echo '开始启动一堆东西...'
step1 &
step2 &
step3 &
docker start ngui &
docker start static &
docker start mysql57 &
docker start mariadb &
docker start mysql801 &
wait
clear
docker ps -a
echo '一堆东西已经启动完毕'
