#!/bin/bash

:<<EOF
排除指定文件复制所有
Version: 1.0
Update: 2022-12-12
Create: 2022-12-12
Author: ahat_hua
EOF

echo '排除指定文件复制所有...'
#cp -rf !(.git|LICENSE|cp-to.sh) ~/ienv/
cp -rfv Adblock-Plus.ini .bashrc build-config.sh .gitconfig .gitignore PhraseEdit.txt .poshrc Preferences.sublime-settings README.md smb.conf .ssh vim ~/ienv/
echo '复制完成。'
exit 0

