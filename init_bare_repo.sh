#!/bin/bash

:<<EOF
服务端快速创建Git裸仓库脚本
需要root用户，非root创建失败
Version: 1.0 for unix
Update: 2022-09-17
Author: ahat_hua
EOF

read -p "创建Git仓库，输入名称：" repo_name
if [ $repo_name ]; then
    repo_name_git="${repo_name}.git"
else
    echo '仓库名称不符合'
    exit 0
fi
cd /home/git/
mkdir $repo_name_git
git init --bare $repo_name_git
chown -R git:git $repo_name_git
cd -
exit 0

