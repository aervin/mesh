#!/bin/bash

:<<EOF
生成项目目录树文件
并添加注释文本
脚本依赖: tree, sed, bash

Version: 1.0 for Linux bash
Update: 2022-10-02
Author: ahat_hua
EOF

# sed 正则参考
# 命令行使用
# sed -r -n "s/^\.$/&\/  #项目根目录 /p" .treefile
# 修改到文件里使用
# sed -r -i "s/^\.$/&\/  #项目根目录 /" .treefile
# sed -r -i "s/\.gitignore$/&  #Git忽略配置 /" .treefile
# sed -r -i "s/^(\||\`)-- webpack\.config\.js$/&  #webpack配置文件 /" .treefile

# 生成目录树
tree -aF -I "node_modules|.git|dist" > .treefile

# 目录树添加公共注释规则
sed -r -i "s/^\.$/&\/  #项目根目录 /" .treefile
sed -r -i "s/^(\||\`)-- \.treefile$/&  #目录树文本数据 /" .treefile
sed -r -i "s/\.gitignore$/&  #Git忽略配置 /" .treefile
sed -r -i "s/^(\||\`)-- package\.json$/&  #npm依赖配置 /" .treefile
sed -r -i "s/^(\||\`)-- package-lock\.json$/&  #npm依赖版本锁定 /" .treefile
sed -r -i "s/^(\||\`)-- pnpm-lock\.yaml$/&  #pnpm依赖版本锁定 /" .treefile
sed -r -i "s/^(\||\`)-- yarn\.lock$/&  #yarn依赖版本锁定 /" .treefile
sed -r -i "s/^(\||\`)-- README\.md$/&  #自述文件 /" .treefile
sed -r -i "s/^(\||\`)-- webpack\.config\.js$/&  #webpack配置文件 /" .treefile
sed -r -i "s/^(\||\`)-- src\/$/&  #源码默认目录 /" .treefile
sed -r -i "s/^(\||\`)-- dist\/$/&  #默认构建目录 /" .treefile
sed -r -i "s/^(\||\`)-- tree-build\.sh$/&  #目录树添加注释脚本 /" .treefile

# 在后面添加自定义的目录文件注释规则
